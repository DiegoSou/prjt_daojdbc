# Prjt_DaoJdbc

Projeto usando conceito de Data Access Object em conexão com banco de dados JDBC/MySQL

# Tabelas Seller e Department
	
Seller:
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(60) NOT NULL,
email VARCHAR(60) NOT NULL,
birthDate DATE NOT NULL,
baseSalary DECIMAL,
departmentId INT NOT NULL,
PRIMARY KEY(id),
FOREIGN KEY(departmentId) REFERENCES department(DepartmentId)

Department:
DepartmentId INT NOT NULL AUTO_INCREMENT,
DepartmentName VARCHAR(60) NOT NULL UNIQUE,
PRIMARY KEY(DepartmentId)
