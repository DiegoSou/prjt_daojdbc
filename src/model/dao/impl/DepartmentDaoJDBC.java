package model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import control.db.DbConnector;
import control.db.MySQLException;
import model.dao.DepartmentDao;
import model.entities.Department;

public class DepartmentDaoJDBC implements DepartmentDao {

	private Connection conn;
	
	public DepartmentDaoJDBC(Connection conn) {
		this.conn = conn;
	}
	@Override
	public void insert(Department obj) {
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.prepareStatement(
					"INSERT INTO department(DepartmentName)"
					+ " VALUES(?)", Statement.RETURN_GENERATED_KEYS);
			
			st.setString(1, obj.getName());
			
			int rowsAffec = st.executeUpdate();
			
			if(rowsAffec > 0) {
				rs = st.getGeneratedKeys();
				if(rs.next()) {
					obj.setId(rs.getInt(1));
				}
			}else {
				throw new SQLException("[-] Erro ao inserir dados. (Nenhuma linha afetada)");
			}
		}
		catch (SQLException e) {
			throw new MySQLException(e.getMessage());
		}
		finally {
			DbConnector.closeStatement(st);
			DbConnector.closeResultSet(rs);
		}
	}

	@Override
	public void update(Department obj) {
		PreparedStatement st = null;
		
		try {
			st = conn.prepareStatement(
					"UPDATE department"
					+ " SET DepartmentName = ?"
					+ " WHERE DepartmentId = ?");
			
			st.setString(1, obj.getName());
			st.setInt(2, obj.getId());
			
			int rowsAffec = st.executeUpdate();
			
			if(rowsAffec > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + rowsAffec);
			}else {
				throw new SQLException("Não foi encontrado nenhum departamento com este id.");
			}
		}
		catch (SQLException e) {
			throw new MySQLException(e.getMessage());
		}
		finally {
			DbConnector.closeStatement(st);
		}
	}

	@Override
	public void deleteById(Integer id) {
		PreparedStatement st = null;
		
		try {
			st = conn.prepareStatement("DELETE FROM department WHERE DepartmentId = ?");
			
			st.setInt(1, id);
			
			int rowsAffec = st.executeUpdate();
			
			if(rowsAffec > 0) {
				System.out.println("Atualização realizada! Linhas afetadas: " + rowsAffec);
			}else {
				throw new SQLException("Não foi encontrado nenhum departamento com este id.");
			}
		}
		catch (SQLException e) {
			throw new MySQLException(e.getMessage());
		}
	}

	@Override
	public Department findById(Integer id) {
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.prepareStatement(
					"SELECT department.*, DepartmentName AS DepName"
					+ " FROM department WHERE DepartmentId =  ?");
			st.setInt(1, id);
			
			rs = st.executeQuery();
			
			if(rs.next()) {
				Department dep = instantiateDepartment(rs);
				return dep;
			} else {
				throw new SQLException("Não há departamentos com este id.");
			}
		}
		catch (SQLException e) {
			throw new MySQLException(e.getMessage());
		}
		finally {
			DbConnector.closeStatement(st);
			DbConnector.closeResultSet(rs);
		}
	}

	@Override
	public List<Department> findAll() {
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			st = conn.prepareStatement(
					"SELECT DepartmentId, DepartmentName AS DepName"
					+ " FROM department"
					+ " ORDER BY DepartmentId");
			
			rs = st.executeQuery();
			
			List<Department> list = new ArrayList<>();
			while(rs.next()) {
				Department dep = instantiateDepartment(rs);
				list.add(dep);
			}
			
			return list;
		}
		catch (SQLException e) {
			throw new MySQLException(e.getMessage());
		}
		finally {
			DbConnector.closeStatement(st);
			DbConnector.closeResultSet(rs);
		}
	}
	
	private Department instantiateDepartment(ResultSet rs) throws SQLException {
		Department dep = new Department();
		dep.setId(rs.getInt("DepartmentId"));
		dep.setName(rs.getString("DepName"));
		return dep;
	}
}
