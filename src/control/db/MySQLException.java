package control.db;

public class MySQLException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MySQLException(String msg) {
		super(msg);
	}
}
