package application;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

import model.dao.DaoFactory;
import model.dao.SellerDao;
import model.entities.Department;
import model.entities.Seller;

public class Program {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		SellerDao sellerDao = DaoFactory.createSellerDao();
		
		System.out.printf("===== TEST 1: FindById ===== %n");
		Seller seller = sellerDao.findById(2);
		
		System.out.println(seller);
		
		System.out.printf("%n ===== TEST 2: FindByDepartment ===== %n");
		Department department = new Department(2, null);
		List<Seller> list = sellerDao.findByDepartment(department);
		
		for (Seller s : list) {
			System.out.println(s);
		}
		
		System.out.printf("%n ===== TEST 3: FindAll ===== %n");
		for (Seller obj : sellerDao.findAll()) {
			System.out.println(obj);
		}
		
		System.out.printf("%n ==== TEST 4: Insert ===== %n");
		Seller newSeller = new Seller(null, "Greg", "greg@gmail.com", new Date(), 4000.0, department);
		sellerDao.insert(newSeller);
		System.out.println("Inserido! Novo id de funcionário: " + newSeller.getId());
		
		System.out.printf("%n ==== TEST 5: Update ===== %n");
		seller = sellerDao.findById(1);
		seller.setName("Martha Waine");
		sellerDao.update(seller);
		
		System.out.printf("%n ==== TEST 6: Delete ===== %n");
		System.out.print("Deletar funcionário (id): ");
		int id = scan.nextInt();
		sellerDao.deleteById(id);
		
		
		scan.close();
	}

}
