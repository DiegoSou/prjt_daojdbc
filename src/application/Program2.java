package application;

import java.util.Scanner;

import model.dao.DaoFactory;
import model.dao.DepartmentDao;
import model.entities.Department;

public class Program2 {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		DepartmentDao depDao = DaoFactory.createDepartmentDao();
		
		// FindById
		System.out.printf("%n ==== TEST 1: findById ===== %n");
		Department dep = depDao.findById(2);
		
		System.out.println(dep);
		
		// Insert
		System.out.printf("%n ==== TEST 2: insert ===== %n");
		Department newDep = new Department(null, "Testing");
		depDao.insert(newDep);
		
		System.out.println("Novo departamento inserido! Identificador: " + newDep.getId());
		
		// FindAll
		System.out.printf("%n ==== TEST 3: findAll ===== %n");
		for(Department dp : depDao.findAll()) {
			System.out.println(dp);
		}
		
		// Update
		System.out.printf("%n ==== TEST 4: update ===== %n");
		dep = depDao.findById(3);
		dep.setName("Social Media");
		depDao.update(dep);
		
		// Delete
		System.out.printf("%n ==== TEST 5: delete ===== %n");
		System.out.print("Deletar departamento (id): ");
		int id = scan.nextInt();
		depDao.deleteById(id);
		
		scan.close();
	}
}
